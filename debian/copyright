Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Colobot
Source: http://github.com/colobot/colobot

Files: *
Copyright: 2001-2025, Daniel Roux, EPSITEC SA & TerranovaTeam
License: GPL-3+

Files: debian/*
Copyright: 2013-2016, 2024-2025, Didier Raboud <odyx@debian.org>
License: GPL-3+

Files: desktop/info.colobot.Colobot.appdata.xml
Copyright: 2018, TerranovaTeam
License: CC0-1.0
 On Debian systems the full text of the CC0-1.0 license can be found in
 /usr/share/common-licenses/CC0-1.0.

Files: data/fonts/*
Copyright: 2003, Bitstream, Inc. Bitstream Vera
License: GPL-3+

Files: lib/*
Copyright: 2005-2009, Warzone Resurrection Project
  1999-2004, Eidos Interactive
License: GPL-2+

Files: lib/hippomocks/*
Copyright: 2008, Bas van Tiel, Christian Rexwinkel, Mike Looijmans, Peter Bindels
License: LGPL-2.1+

Files: lib/localename/*
Copyright: 1995-2013, Free Software Foundation, Inc
License: LGPL-2.1+

Files: lib/wingetopt/*
Copyright: 2002, Todd C. Miller <Todd.Miller@courtesan.com>
License: BSD-2-clause or ISC

Files: src/*
Copyright: 2001-2025, Daniel Roux, EPSITEC SA & TerranovaTeam
License: GPL-3+

Files: test/*
Copyright: 2001-2025, Daniel Roux, EPSITEC SA & TerranovaTeam
License: GPL-3+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser General
 Public License can be found in '/usr/share/common-licenses/LGPL-2.1'.
